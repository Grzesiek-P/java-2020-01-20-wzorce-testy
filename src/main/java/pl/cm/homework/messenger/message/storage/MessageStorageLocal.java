package pl.cm.homework.messenger.message.storage;

import pl.cm.homework.messenger.message.model.Message;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MessageStorageLocal implements MessageStorage {
    private static final Logger log = Logger.getLogger(MessageStorageLocal.class.getName());

    private final File storage;



    public MessageStorageLocal(File storage) {
        this.storage = storage;
        if (!this.storage.exists()) {
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.storage))){
                oos.writeInt(0);

            } catch (IOException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }

    @Override
    public boolean add(Message message) {
        List<Message> messages = new LinkedList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storage))) {
            if (ois.readInt() != 0) {
                messages = (List<Message>) ois.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }

        boolean result = messages.add(message);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storage))) {
            oos.writeInt(messages.size());
            oos.writeObject(messages);
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public boolean remove(Message message) {
        List<Message> messages = new LinkedList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storage))) {
            if (ois.readInt() != 0) {
                messages = (List<Message>) ois.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }

        boolean result = messages.remove(message);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storage))) {
            oos.writeInt(messages.size());
            oos.writeObject(messages);
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public List<Message> getAllMessages() {
        List<Message> messages = new LinkedList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storage))) {
            if (ois.readInt() != 0) {
                messages = (List<Message>) ois.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return messages;
    }
}
