package pl.cm.homework.messenger.message.model;

import pl.cm.homework.messenger.user.model.User;

import java.time.LocalDateTime;
import java.util.logging.Logger;

public class MessageBuilder {
    private static final Logger log = Logger.getLogger(MessageBuilder.class.getName());

    private String title;
    private User author;
    private String content;
    private LocalDateTime creationDate;

    public MessageBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public MessageBuilder withAuthor(User author) {
        this.author = author;
        return this;
    }

    public MessageBuilder withContent(String content) {
        this.content = content;
        return this;
    }

    public MessageBuilder withCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public Message build() throws NullPointerException {
            checkAllFieldsForNull();
            return new Message(title, author, content, creationDate);
    }

    private void checkAllFieldsForNull() throws NullPointerException {
            checkTitleForNull();
            checkAuthorForNull();
            checkContentForNull();
            checkDateForNull();
    }

    private void checkTitleForNull() throws NullPointerException{
        if(title == null) {
            throw new NullPointerException("tile is null");
        }
    }

    private void checkAuthorForNull() throws NullPointerException{
        if(author == null) {
            throw new NullPointerException("author is null");
        }
    }

    private void checkContentForNull() throws NullPointerException{
        if(content == null) {
            throw new NullPointerException("content is null");
        }
    }

    private void checkDateForNull() throws NullPointerException{
        if(creationDate == null) {
            throw new NullPointerException("date is null");
        }
    }
}
