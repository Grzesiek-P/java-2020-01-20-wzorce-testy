package pl.cm.homework.messenger.message.storage;

import org.junit.*;


import static org.assertj.core.api.Assertions.*;


import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.user.model.User;

import java.io.*;
import java.time.LocalDateTime;


/*
As mockito cannot mock final methods in ObjectInputStream and ObjectOutputStream,
I have decided to make this more of an integration test
 */

public class MessageStorageLocalTest {

    private File file = new File("message-storage-test");


    private Message message1 = new Message("test1", new User("TestUser1"), "test content 1", LocalDateTime.now());
    private Message message2 = new Message("test3", new User("TestUser3"), "test content 3", LocalDateTime.now());

    @Before
    public void before() throws Exception {
        file.delete();
    }

    @Test
    public void add_remove_getAllMessages_IntegrationTest() throws IOException {
    MessageStorageLocal storage = new MessageStorageLocal(file);

    assertThat(file.exists()).isTrue();

    Boolean resultAdd1 = storage.add(message1);
    Boolean resultAdd2 = storage.add(message2);

    assertThat(storage.getAllMessages()).containsExactlyInAnyOrder(message1, message2);
    assertThat(resultAdd1).isTrue();
    assertThat(resultAdd2).isTrue();

    Boolean resultRemove1 =  storage.remove(message2);
    Boolean resultRemove2 =  storage.remove(message2);

    assertThat(storage.getAllMessages()).containsExactly(message1);
    assertThat(resultRemove1).isTrue();
    assertThat(resultRemove2).isFalse();


    }

    @After
    public void after() {
        file.delete();
    }


}