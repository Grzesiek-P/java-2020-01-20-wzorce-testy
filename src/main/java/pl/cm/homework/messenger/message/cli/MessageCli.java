package pl.cm.homework.messenger.message.cli;

import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.message.printer.MessagePrinter;
import pl.cm.homework.messenger.message.printer.PrinterProvider;
import pl.cm.homework.messenger.message.service.MessageProvider;
import pl.cm.homework.messenger.message.service.MessageRepository;
import pl.cm.homework.messenger.message.service.MessageStreamFilter;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class MessageCli {

    private final MessageRepository repository;
    private final MessageProvider messageProvider;
    private final PrinterProvider printerProvider;
    private final Scanner scanner;

    public MessageCli(MessageRepository repository,
                      MessageProvider provider,
                      PrinterProvider printerProvider,
                      Scanner scanner) {
        this.repository = repository;
        this.messageProvider = provider;
        this.printerProvider = printerProvider;
        this.scanner = scanner;
    }

    public String parseCommandAndExecute() {
        String command = parseCommand();

        if ("add".equals(command)) {
            executeAdd();
        } else if ("remove".equals(command)) {
            executeDelete();
        } else if ("all".equals(command)) {
            printMessages(repository.getAllMessages());
        } else if ("filter".equals(command)) {
            executeFilter();
        }
        return command;
    }

    private String parseCommand() {
        printInstructions();
        return scanner.next();
    }

    private void printInstructions() {
        System.out.println("-------------------------");
        System.out.println("-------------------------");
        System.out.println("- [add] new message,");
        System.out.println("- [remove] message(s),");
        System.out.println("- display [all] messages,");
        System.out.println("- [filter] messages to display,");
        System.out.println("- [quit] program.");
    }

    private void executeAdd() {
        repository.add(messageProvider.getMessageFromUser());
        System.out.println("Message created");
    }

    private void executeDelete() {
        List<Message> messages = new LinkedList<>();
        System.out.println("Delete [all] messages or [filter]");
        String command = scanner.next();
        if ("all".equals(command)) {
            messages = repository.getAllMessages();
        } else if ("filter".equals(command)) {
            messages = filterMessages();
        }

        if (messages.isEmpty()) {
            System.out.println("No messages to remove");
        } else {
            System.out.println("Following messages will be removed:");
            printerProvider.createPrinterShort().printList(messages);
            System.out.println("Are you sure? [y/n]");
            if ("y".equals(scanner.next())) {
                messages.forEach(repository::remove);
                System.out.println("Messages removed");
            }
        }
    }

    private void printMessages(List<Message> messages) {
        parsePrinter().printList(messages);
    }

    private MessagePrinter parsePrinter() {
        System.out.println("Display method [short] or [full]");
        String displayMethod = "";
        while (!"short".equals(displayMethod) && !"full".equals(displayMethod)) {
            displayMethod = scanner.next();
        }
        if ("short".equals(displayMethod)) {
            return printerProvider.createPrinterShort();
        } else {
            return printerProvider.createPrinterLong();
        }
    }

    private void executeFilter() {
        printMessages(filterMessages());
    }

    private List<Message> filterMessages() {
        String command = "";
        MessageStreamFilter filter = new MessageStreamFilter(repository.getStream());
        while (!"end".equals(command)) {
            command = parseFilterCommand();
            if ("author".equals(command)) {
                filter = filterMessagesByAuthor(filter);
            } else if ("date".equals(command)) {
                filter = filterMessagesByDate(filter);
            } else if ("title".equals(command)) {
                filter = filterMessagesByTitle(filter);
            } else if ("content".equals(command)) {
                filter = filterMessagesByContent(filter);
            }
        }
        return filter.getMessageList();
    }

    private String parseFilterCommand() {
        System.out.println("Filter messages by [author], [date], [title] or [content].");
        System.out.println("If done [end] filtering.");
        return scanner.next();
    }

    private MessageStreamFilter filterMessagesByAuthor(MessageStreamFilter filter) {
        System.out.println("Messages written by:");
        filter.filterByAuthor(scanner.next());
        return filter;
    }

    private MessageStreamFilter filterMessagesByDate(MessageStreamFilter filter) {
        System.out.println("Messages written on [YYYY-MM-DD]:");
        try {
            filter.filterByDate(LocalDate.parse(scanner.next()));
        } catch (DateTimeParseException ex) {
            System.out.println("Invalid date format");
        }
        return filter;
    }

    private MessageStreamFilter filterMessagesByTitle(MessageStreamFilter filter) {
        System.out.println("Messages containing in their title:");
        filter.filterByTitle(scanner.next());
        return filter;
    }

    private MessageStreamFilter filterMessagesByContent(MessageStreamFilter filter) {
        System.out.println("Messages containing:");
        filter.filterByContent(scanner.next());
        return filter;
    }



}
