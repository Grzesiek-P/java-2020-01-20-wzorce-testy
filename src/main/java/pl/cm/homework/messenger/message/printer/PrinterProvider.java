package pl.cm.homework.messenger.message.printer;

public class PrinterProvider {

    public MessagePrinter createPrinterShort() {
        return new MessagePrinterShort();
    }

    public MessagePrinter createPrinterLong() {
        return new MessagePrinterLong();
    }

}
