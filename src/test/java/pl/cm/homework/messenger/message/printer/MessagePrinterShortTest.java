package pl.cm.homework.messenger.message.printer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.user.model.User;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MessagePrinterShortTest {


    @Mock
    private Message message1;
    @Mock
    private Message message2;
    @Mock
    private User user1;
    @Mock
    private User user2;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);


        when(message1.getTitle()).thenReturn("Test title 1");
        when(message1.getAuthor()).thenReturn(user1);


        when(message2.getTitle()).thenReturn("Test title 2");
        when(message2.getAuthor()).thenReturn(user2);


        when(user1.getLogin()).thenReturn("user1");
        when(user2.getLogin()).thenReturn("user2");


    }

    @Test
    public void print_callsGetters_givenMessage() {
        MessagePrinterShort messagePrinterShort = new MessagePrinterShort();

        messagePrinterShort.print(message1);

        verify(message1, times(1)).getTitle();
        verify(message1, times(1)).getAuthor();
    }

    @Test
    public void printList_printsAllMessages_givenList() {
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        MessagePrinterShort messagePrinterShort = new MessagePrinterShort();

        messagePrinterShort.printList(list);

        verify(message1, times(1)).getTitle();
        verify(message1, times(1)).getAuthor();
        verify(message2, times(1)).getTitle();
        verify(message2, times(1)).getAuthor();
    }

    @Test
    public void printList_printsMessage_givenEmptyList() {
        List<Message> list = new LinkedList<>();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        MessagePrinterShort messagePrinterShort = new MessagePrinterShort();

        messagePrinterShort.printList(list);

        String outputLine = new String(bos.toByteArray());
        assertThat(outputLine.contains("No messages to print"));

    }


}