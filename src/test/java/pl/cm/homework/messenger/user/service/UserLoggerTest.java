package pl.cm.homework.messenger.user.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.cm.homework.messenger.user.model.User;
import pl.cm.homework.messenger.user.storage.UserStorage;

import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserLoggerTest {

    @Mock
    private UserStorage storage;


    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getLoggedUser_addsUserAndReturnsUser_whenNotInDatabase() {
        when(storage.getAllUsers()).thenReturn(new HashSet<>());
        User user = new User("username");
        System.setIn(new ByteArrayInputStream("username\n".getBytes()));
        Scanner scanner = new Scanner(System.in);
        UserLogger logger = new UserLogger(storage, scanner);

        User loggedUser = logger.getLoggedUser();

        ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
        verify(storage).add(captor.capture());
        assertThat(captor.getValue()).isEqualTo(user);
        assertThat(loggedUser).isEqualTo(user);
    }

    @Test
    public void getLoggedUser_returnsUser_ifInDatabase() {
        User user = new User("username");
        System.setIn(new ByteArrayInputStream("username\n".getBytes()));
        Set<User> userSet = new HashSet<>();
        userSet.add(user);
        when(storage.getAllUsers()).thenReturn(userSet);
        Scanner scanner = new Scanner(System.in);
        UserLogger logger = new UserLogger(storage, scanner);

        User loggedUser = logger.getLoggedUser();

        verify(storage, times(0)).add(user);
        assertThat(loggedUser).isEqualTo(user);

    }

}