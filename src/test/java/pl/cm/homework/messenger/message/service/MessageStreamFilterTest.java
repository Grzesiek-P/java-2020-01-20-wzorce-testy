package pl.cm.homework.messenger.message.service;

import org.junit.Before;
import org.junit.Test;
import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.user.model.User;

import java.awt.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;


import static org.mockito.Mockito.*;

import static org.assertj.core.api.Assertions.*;

public class MessageStreamFilterTest {


        private User user1 = new User("user1");
        private User user2 = new User("user2");
        private User user3 = new User("user3");

        private LocalDate date1 = LocalDate.parse("2020-01-01");
        private LocalDate date2 = LocalDate.parse("2019-01-01");
        private LocalDate date3 = LocalDate.parse("2018-01-01");

        private String title1 = "Test title 1";
        private String title2 = "Test title 2";

        private String content1 = "Test content 1";
        private String content2 = "Test content 2";

        private Message message1 = new Message(title1, user1, content1, date1.atStartOfDay());
        private Message message2 = new Message(title2, user2, content2, date2.atStartOfDay());

        private List<Message> list = Arrays.asList(message1, message2);

        private Stream<Message> stream = list.stream();



    @Test
    public void getAllMessages_returnsListOfAllMessages() {
        MessageStreamFilter messageStreamFilter = new MessageStreamFilter(stream);

        List<Message> result = messageStreamFilter.getMessageList();

        assertThat(result).containsExactly(message1, message2);
    }

    @Test
    public void filterByDate_filtersByDate_givenProperDate() {
        MessageStreamFilter messageStreamFilter = new MessageStreamFilter(stream);

        messageStreamFilter.filterByDate(date1);

        assertThat(messageStreamFilter.getMessageList()).containsExactly(message1);

    }

    @Test
    public void filterByDate_returnsEmpty_givenWrongDate() {
        MessageStreamFilter messageStreamFilter = new MessageStreamFilter(stream);

        messageStreamFilter.filterByDate(date3);

        assertThat(messageStreamFilter.getMessageList()).isEmpty();

    }

    @Test
    public void filterByAuthor_filtersByAuthor_givenProperAuthor() {
        MessageStreamFilter messageStreamFilter = new MessageStreamFilter(stream);

        messageStreamFilter.filterByAuthor(user1.getLogin());

        assertThat(messageStreamFilter.getMessageList()).containsExactly(message1);

    }

    @Test
    public void filterByAuthor_returnsEmpty_givenWrongAuthor() {
        MessageStreamFilter messageStreamFilter = new MessageStreamFilter(stream);

        messageStreamFilter.filterByAuthor(user3.getLogin());

        assertThat(messageStreamFilter.getMessageList()).isEmpty();

    }

    @Test
    public void filterByTitle_filtersByTitle_givenProperString() {
        MessageStreamFilter messageStreamFilter = new MessageStreamFilter(stream);

        messageStreamFilter.filterByTitle("1");

        assertThat(messageStreamFilter.getMessageList()).containsExactly(message1);

    }

    @Test
    public void filterByTitle_returnsEmpty_givenWrongString() {
        MessageStreamFilter messageStreamFilter = new MessageStreamFilter(stream);

        messageStreamFilter.filterByTitle("3");

        assertThat(messageStreamFilter.getMessageList()).isEmpty();

    }

    @Test
    public void filterByContent_filtersByContent_givenProperString() {
        MessageStreamFilter messageStreamFilter = new MessageStreamFilter(stream);

        messageStreamFilter.filterByContent("1");

        assertThat(messageStreamFilter.getMessageList()).containsExactly(message1);

    }

    @Test
    public void filterByContent_returnsEmpty_givenWrongString() {
        MessageStreamFilter messageStreamFilter = new MessageStreamFilter(stream);

        messageStreamFilter.filterByContent("3");

        assertThat(messageStreamFilter.getMessageList()).isEmpty();

    }

}