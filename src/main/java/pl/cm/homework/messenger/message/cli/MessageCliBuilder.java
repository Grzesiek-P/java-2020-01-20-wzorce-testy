package pl.cm.homework.messenger.message.cli;

import pl.cm.homework.messenger.message.printer.PrinterProvider;
import pl.cm.homework.messenger.message.service.MessageProvider;
import pl.cm.homework.messenger.message.service.MessageRepository;

import java.util.Scanner;

public class MessageCliBuilder {

    private MessageRepository repository;
    private MessageProvider messageProvider;
    private PrinterProvider printerProvider;
    private Scanner scanner;

    public MessageCliBuilder withMessageRepository(MessageRepository repository) {
        this.repository = repository;
        return this;
    }

    public MessageCliBuilder withMessageProvider(MessageProvider messageProvider) {
        this.messageProvider = messageProvider;
        return this;
    }

    public MessageCliBuilder withPrinterProvider(PrinterProvider printerProvider) {
        this.printerProvider = printerProvider;
        return this;
    }

    public MessageCliBuilder withScanner(Scanner scanner) {
        this.scanner = scanner;
        return this;
    }

    public MessageCli build() {
        return new MessageCli(repository, messageProvider, printerProvider, scanner);
    }
}
