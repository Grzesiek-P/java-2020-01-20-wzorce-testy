package pl.cm.homework.messenger.message.printer;

import pl.cm.homework.messenger.message.model.Message;

import javax.swing.text.DateFormatter;
import java.time.format.DateTimeFormatter;

public class MessagePrinterLong implements MessagePrinter{

    String DATE_FORMATTER= "yyyy-MM-dd HH:mm";
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
    @Override
    public void print(Message message) {


        System.out.println("------------");
        System.out.println("Created by " + message.getAuthor().getLogin() + " on " + message.getCreationDate().format(dateTimeFormatter));
        System.out.println("-----");
        System.out.println("TITLE:");
        System.out.println(message.getTitle());
        System.out.println("CONTENT:");
        System.out.println(message.getContent());
    }
}
