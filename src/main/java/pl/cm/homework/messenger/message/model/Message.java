package pl.cm.homework.messenger.message.model;

import pl.cm.homework.messenger.user.model.User;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Message implements Serializable {

    private static final long serialVersionUID = 1;

    private final String title;
    private final User author;
    private final String content;
    private final LocalDateTime creationDate;

    public Message(String title, User author, String content, LocalDateTime creationDate) {
        this.title = title;
        this.author = author;
        this.content = content;
        this.creationDate = creationDate;
    }


    public String getTitle() {
        return title;
    }

    public User getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(title, message.title) &&
                Objects.equals(author, message.author) &&
                Objects.equals(content, message.content) &&
                Objects.equals(creationDate, message.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, content, creationDate);
    }

    @Override
    public String toString() {
        return "Message{" +
                "title='" + title + '\'' +
                ", author=" + author +
                ", content='" + content + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
