package pl.cm.homework.messenger.message.service;

import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.message.storage.MessageStorage;

import java.util.List;
import java.util.stream.Stream;

public class MessageRepository {

    private final MessageStorage storage;

    public MessageRepository(MessageStorage storage) {
        this.storage = storage;
    }

    public boolean add(Message message) {
        return storage.add(message);
    }

    public boolean remove(Message message) {
        return storage.remove(message);
    }

    public List<Message> getAllMessages() {
        return storage.getAllMessages();
    }

    public Stream<Message> getStream() {
        return storage.getAllMessages().stream();
    }
}
