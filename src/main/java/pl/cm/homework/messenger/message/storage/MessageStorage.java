package pl.cm.homework.messenger.message.storage;

import pl.cm.homework.messenger.message.model.Message;

import java.util.List;

public interface MessageStorage {

    boolean add(Message message);
    boolean remove(Message message);
    List<Message> getAllMessages();
}
