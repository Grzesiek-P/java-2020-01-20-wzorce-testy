package pl.cm.homework.messenger;

import pl.cm.homework.messenger.message.cli.MessageCli;
import pl.cm.homework.messenger.message.cli.MessageCliBuilder;
import pl.cm.homework.messenger.message.printer.PrinterProvider;
import pl.cm.homework.messenger.message.service.MessageProvider;
import pl.cm.homework.messenger.message.service.MessageRepository;
import pl.cm.homework.messenger.message.storage.MessageStorage;
import pl.cm.homework.messenger.message.storage.MessageStorageLocal;
import pl.cm.homework.messenger.message.storage.MessageStorageMemory;
import pl.cm.homework.messenger.user.model.User;
import pl.cm.homework.messenger.user.service.UserLogger;
import pl.cm.homework.messenger.user.storage.UserStorage;
import pl.cm.homework.messenger.user.storage.UserStorageLocal;

import java.io.File;
import java.util.Scanner;

public class MessengerMain {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        File userStorageFile = new File("user-storage");
        UserStorage userStorage = new UserStorageLocal(userStorageFile);
        UserLogger userLogger = new UserLogger(userStorage, scanner);

        User user = userLogger.getLoggedUser();

        //For local storage of messages
        File messageStorageFile = new File("message-storage");
        MessageStorage messageStorage = new MessageStorageLocal(messageStorageFile);

        //For memory storage of messages
//        MessageStorage messageStorage = new MessageStorageMemory();

        MessageRepository messageRepository = new MessageRepository(messageStorage);
        MessageProvider messageProvider = new MessageProvider(user, scanner);
        PrinterProvider printerProvider = new PrinterProvider();
        MessageCli messageController = new MessageCliBuilder()
                .withMessageRepository(messageRepository)
                .withMessageProvider(messageProvider)
                .withPrinterProvider(printerProvider)
                .withScanner(scanner)
                .build();

        String command = null;
        while (!"quit".equals(command)) {
            command = messageController.parseCommandAndExecute();
        }
    }
}
