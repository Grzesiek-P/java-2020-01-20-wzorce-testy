package pl.cm.homework.messenger.message.service;

import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.user.model.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MessageStreamFilter {

    private Stream<Message> stream;

    public MessageStreamFilter(Stream<Message> stream) {
        this.stream = stream;
    }

    public MessageStreamFilter filterByDate(LocalDate date) {
        stream = stream.filter(message -> message.getCreationDate().toLocalDate().equals(date));
        return this;
    }

    public MessageStreamFilter filterByAuthor(String login) {
        stream = stream.filter(message -> message.getAuthor().getLogin().equals(login));
        return this;
    }

    public MessageStreamFilter filterByTitle(String snippet) {
        stream = stream.filter(message -> message.getTitle().contains(snippet));
        return this;
    }

    public MessageStreamFilter filterByContent(String snippet) {
        stream = stream.filter(message -> message.getContent().contains(snippet));
        return this;
    }

    public List<Message> getMessageList() {
        return stream.collect(Collectors.toList());
    }
}
