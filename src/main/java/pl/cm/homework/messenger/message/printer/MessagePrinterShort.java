package pl.cm.homework.messenger.message.printer;

import pl.cm.homework.messenger.message.model.Message;

public class MessagePrinterShort implements MessagePrinter {
    @Override
    public void print(Message message) {
        System.out.println("------------");
        System.out.println(message.getTitle());
        System.out.println("Created by " + message.getAuthor().getLogin());
    }
}
