package pl.cm.homework.messenger.user.storage;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import pl.cm.homework.messenger.user.model.User;

import static org.assertj.core.api.Assertions.*;

public class UserStorageMemoryTest {


    @Mock
    private User user;
    @Before
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void add_addsUser_ifNotInMemory() {
        UserStorageMemory storage = new UserStorageMemory();

        storage.add(user);

        assertThat(storage.getAllUsers()).containsExactly(user);
    }

    @Test
    public void add_addsUser_ifAlreadyInMemory() {
        UserStorageMemory storage = new UserStorageMemory();

        storage.add(user);
        Boolean result = storage.add(user);

        assertThat(result).isFalse();
    }

    @Test
    public void remove_removesMessage() {
        UserStorageMemory storage = new UserStorageMemory();

        storage.add(user);
        storage.remove(user);

        assertThat(storage.getAllUsers()).isEmpty();
    }


}