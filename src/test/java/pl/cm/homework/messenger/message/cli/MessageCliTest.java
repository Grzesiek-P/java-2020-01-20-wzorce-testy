package pl.cm.homework.messenger.message.cli;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.message.printer.MessagePrinter;
import pl.cm.homework.messenger.message.printer.PrinterProvider;
import pl.cm.homework.messenger.message.service.MessageProvider;
import pl.cm.homework.messenger.message.service.MessageRepository;
import pl.cm.homework.messenger.message.service.MessageStreamFilter;
import pl.cm.homework.messenger.user.model.User;

import java.io.ByteArrayInputStream;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
public class MessageCliTest {

    @Mock
    private MessageRepository repository;
    @Mock
    private MessageProvider messageProvider;
    @Mock
    private PrinterProvider printerProvider;
    @Mock
    private Message message1;
    @Mock
    private Message message2;
    @Mock
    private MessagePrinter printer;
    @Mock
    private MessageStreamFilter filter;
    @Mock
    Stream<Message> stream;

    @Before
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(printerProvider.createPrinterShort()).thenReturn(printer);
        when(printerProvider.createPrinterLong()).thenReturn(printer);
        when(repository.getStream()).thenReturn(stream);
        whenNew(MessageStreamFilter.class).withArguments(stream).thenReturn(filter);

    }

    @Test
    public void parseCommandAndExecute_addsMessage() {
        System.setIn(new ByteArrayInputStream("add".getBytes()));
        Scanner scanner = new Scanner(System.in);
        when(messageProvider.getMessageFromUser()).thenReturn(message1);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        verify(repository).add(message1);
        verify(messageProvider).getMessageFromUser();
    }

    @Test
    public void parseCommandAndExecute_removesAllMessages_whenAgreed() {
        System.setIn(new ByteArrayInputStream("remove\nall\ny".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(repository.getAllMessages()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
        verify(repository, times(2)).remove(captor.capture());
        assertThat(captor.getAllValues()).containsExactlyInAnyOrder(message1, message2);
    }

    @Test
    public void parseCommandAndExecute_removesNoMessages_whenDeclined() {
        System.setIn(new ByteArrayInputStream("remove\nall\nn".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(repository.getAllMessages()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
        verify(repository, times(1)).getAllMessages();
        verifyNoMoreInteractions(repository);
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_removesFilteredMessages_byTitle() {
        System.setIn(new ByteArrayInputStream("remove\nfilter\ntitle\ntest\nend\ny".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
        verify(filter).filterByTitle("test");
        verify(repository, times(2)).remove(captor.capture());
        assertThat(captor.getAllValues()).containsExactlyInAnyOrder(message1, message2);
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_removesFilteredMessages_byContent() {
        System.setIn(new ByteArrayInputStream("remove\nfilter\ncontent\ntest\nend\ny".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
        verify(filter).filterByContent("test");
        verify(repository, times(2)).remove(captor.capture());
        assertThat(captor.getAllValues()).containsExactlyInAnyOrder(message1, message2);
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_removesFilteredMessages_byUser() {
        System.setIn(new ByteArrayInputStream("remove\nfilter\nauthor\ntest\nend\ny".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
        verify(filter).filterByAuthor("test");
        verify(repository, times(2)).remove(captor.capture());
        assertThat(captor.getAllValues()).containsExactlyInAnyOrder(message1, message2);
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_removesFilteredMessages_byDate() {
        System.setIn(new ByteArrayInputStream("remove\nfilter\ndate\n2020-01-01\nend\ny".getBytes()));
        Scanner scanner = new Scanner(System.in);
        LocalDate date = LocalDate.parse("2020-01-01");
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
        verify(filter).filterByDate(date);
        verify(repository, times(2)).remove(captor.capture());
        assertThat(captor.getAllValues()).containsExactlyInAnyOrder(message1, message2);
    }


    @Test
    public void parseCommandAndExecute_printsAllMessages_givenShortPrinter() {
        System.setIn(new ByteArrayInputStream("all\nshort".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(repository.getAllMessages()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
    }

    @Test
    public void parseCommandAndExecute_printsAllMessages_givenLongPrinter() {
        System.setIn(new ByteArrayInputStream("all\nfull".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(repository.getAllMessages()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        verify(printerProvider).createPrinterLong();
        verify(printer).printList(list);
    }


    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_filteresMessagesByTitleAndDisplaysShort() {
        System.setIn(new ByteArrayInputStream("filter\ntitle\ntest\nend\nshort".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
        verify(filter).filterByTitle("test");
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_filteresMessagesByTitleAndDisplaysLong() {
        System.setIn(new ByteArrayInputStream("filter\ntitle\ntest\nend\nfull".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(printerProvider).createPrinterLong();
        verify(printer).printList(list);
        verify(filter).filterByTitle("test");
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_filtersMessagesByContentAndDisplaysShort() {
        System.setIn(new ByteArrayInputStream("filter\ncontent\ntest\nend\nshort".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
        verify(filter).filterByContent("test");
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_filtersMessagesByContentAndDisplaysLong() {
        System.setIn(new ByteArrayInputStream("filter\ncontent\ntest\nend\nfull".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        verify(printerProvider).createPrinterLong();
        verify(printer).printList(list);
        verify(filter).filterByContent("test");
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_filtersMessagesByAuthorAndDisplaysShort() {
        System.setIn(new ByteArrayInputStream("filter\nauthor\ntest\nend\nshort".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
        verify(filter).filterByAuthor("test");
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_filtersMessagesByAuthorAndDisplaysLong() {
        System.setIn(new ByteArrayInputStream("filter\nauthor\ntest\nend\nfull".getBytes()));
        Scanner scanner = new Scanner(System.in);
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        verify(printerProvider).createPrinterLong();
        verify(printer).printList(list);
        verify(filter).filterByAuthor("test");
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_filtersMessagesByDateAndDisplaysShort() {
        System.setIn(new ByteArrayInputStream("filter\ndate\n2020-01-01\nend\nshort".getBytes()));
        Scanner scanner = new Scanner(System.in);
        LocalDate date = LocalDate.parse("2020-01-01");
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        verify(printerProvider).createPrinterShort();
        verify(printer).printList(list);
        verify(filter).filterByDate(date);
    }

    @PrepareForTest(MessageCli.class)
    @Test
    public void parseCommandAndExecute_filtersMessagesByDateAndDisplaysLong() {
        System.setIn(new ByteArrayInputStream("filter\ndate\n2020-01-01\nend\nfull".getBytes()));
        Scanner scanner = new Scanner(System.in);
        LocalDate date = LocalDate.parse("2020-01-01");
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        when(filter.getMessageList()).thenReturn(list);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        messageCli.parseCommandAndExecute();

        verify(printerProvider).createPrinterLong();
        verify(printer).printList(list);
        verify(filter).filterByDate(date);
    }

    @Test
    public void ParseCommandAndExecute_returnsQuit_givenQuitCommand() {
        System.setIn(new ByteArrayInputStream("quit".getBytes()));
        Scanner scanner = new Scanner(System.in);
        MessageCli messageCli = new MessageCli(repository, messageProvider, printerProvider, scanner);

        String result = messageCli.parseCommandAndExecute();

        assertThat(result).isEqualTo("quit");
    }


}