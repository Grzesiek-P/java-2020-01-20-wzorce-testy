package pl.cm.homework.messenger.message.printer;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.user.model.User;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;

import static org.assertj.core.api.Assertions.*;

public class MessagePrinterLongTest {


    @Mock
    private Message message1;
    @Mock
    private Message message2;
    @Mock
    private User user1;
    @Mock
    private User user2;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        LocalDateTime date1 = LocalDateTime.now();
        LocalDateTime date2 = LocalDateTime.now();


        when(message1.getTitle()).thenReturn("Test title 1");
        when(message1.getAuthor()).thenReturn(user1);
        when(message1.getCreationDate()).thenReturn(date1);
        when(message1.getContent()).thenReturn("Test content 1");

        when(message2.getTitle()).thenReturn("Test title 2");
        when(message2.getAuthor()).thenReturn(user2);
        when(message2.getCreationDate()).thenReturn(date2);
        when(message2.getContent()).thenReturn("Test content 2");

        when(user1.getLogin()).thenReturn("user1");
        when(user2.getLogin()).thenReturn("user2");


    }

    @Test
    public void print_callsAllGetters_givenMessage() {
        MessagePrinterLong messagePrinterLong = new MessagePrinterLong();

        messagePrinterLong.print(message1);

        verify(message1, times(1)).getTitle();
        verify(message1, times(1)).getCreationDate();
        verify(message1, times(1)).getAuthor();
        verify(message1, times(1)).getContent();
    }

    @Test
    public void printList_printsAllMessages_givenList() {
        List<Message> list = new LinkedList<>();
        list.add(message1);
        list.add(message2);
        MessagePrinterLong messagePrinterLong = new MessagePrinterLong();

        messagePrinterLong.printList(list);

        verify(message1, times(1)).getTitle();
        verify(message1, times(1)).getCreationDate();
        verify(message1, times(1)).getAuthor();
        verify(message1, times(1)).getContent();
        verify(message2, times(1)).getTitle();
        verify(message2, times(1)).getCreationDate();
        verify(message2, times(1)).getAuthor();
        verify(message2, times(1)).getContent();
    }

    @Test
    public void printList_printsMessage_givenEmptyList() {
        List<Message> list = new LinkedList<>();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        MessagePrinterLong messagePrinterLong = new MessagePrinterLong();

        messagePrinterLong.printList(list);

        String outputLine = new String(bos.toByteArray());
        assertThat(outputLine.contains("No messages to print"));

    }

}