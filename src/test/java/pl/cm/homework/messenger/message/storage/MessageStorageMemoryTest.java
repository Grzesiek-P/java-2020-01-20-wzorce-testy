package pl.cm.homework.messenger.message.storage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import static org.assertj.core.api.Assertions.*;

import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pl.cm.homework.messenger.message.model.Message;

import java.util.LinkedList;
import java.util.List;


import static org.mockito.Mockito.*;

public class MessageStorageMemoryTest {

    @Mock
    private Message message;
    @Before
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void add_addsMessage() {
        MessageStorageMemory storage = new MessageStorageMemory();

        storage.add(message);

        assertThat(storage.getAllMessages()).containsExactly(message);
    }

    @Test
    public void remove_removesMessage() {
        MessageStorageMemory storage = new MessageStorageMemory();

        storage.add(message);
        storage.remove(message);

        assertThat(storage.getAllMessages()).isEmpty();
    }

}