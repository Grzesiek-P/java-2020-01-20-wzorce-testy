package pl.cm.homework.messenger.message.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.message.storage.MessageStorage;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

import static org.mockito.Mockito.*;

import static org.assertj.core.api.Assertions.*;


public class MessageRepositoryTest {

    @Mock
    private MessageStorage storage;
    @InjectMocks
    private MessageRepository repository;
    @Before
    public  void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void add_callsAddFromStorage() {
        Message message = mock(Message.class);

        repository.add(message);

        verify(storage, times(1)).add(message);
    }

    @Test
    public void remove_callsRemoveFromStorage() {
        Message message = mock(Message.class);

        repository.remove(message);

        verify(storage, times(1)).remove(message);
    }

    @Test
    public void geAllMessages_callsGetAllMessagesFromStorage() {

        repository.getAllMessages();

        verify(storage, times(1)).getAllMessages();
    }

    @Test
    public void getStream_returnsStreamOfAllMessagesFromStorage() {
        List<Message> list = mock(LinkedList.class);
        when(storage.getAllMessages()).thenReturn(list);

        repository.getStream();

        verify(storage, times(1)).getAllMessages();
        verify(list, times(1)).stream();
    }



}