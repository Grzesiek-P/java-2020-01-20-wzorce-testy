package pl.cm.homework.messenger.message.service;

import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.message.model.MessageBuilder;
import pl.cm.homework.messenger.user.model.User;

import java.time.LocalDateTime;
import java.util.Scanner;

public class MessageProvider {

    private final User author;
    private final Scanner scanner;

    private String title;
    private String content;

    public MessageProvider(User author, Scanner scanner) {
        this.author = author;
        this.scanner = scanner;
    }

    public Message getMessageFromUser() {
        getTitleFromUser();
        getContentFromUser();
        return new MessageBuilder()
                .withTitle(title)
                .withAuthor(author)
                .withContent(content)
                .withCreationDate(LocalDateTime.now())
                .build();

    }

    private void getTitleFromUser() {
        System.out.println("Messqage title:");
        do {
            title = scanner.nextLine();
        } while (title.isBlank());
    }

    private void getContentFromUser() {
        System.out.println("Message content:");
        do {
            content = scanner.nextLine();
        } while (content.isBlank());
    }
}
