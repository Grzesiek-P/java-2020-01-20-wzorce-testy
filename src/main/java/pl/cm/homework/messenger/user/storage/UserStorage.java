package pl.cm.homework.messenger.user.storage;

import pl.cm.homework.messenger.user.model.User;

import java.util.Set;

public interface UserStorage {
    boolean add(User user);
    boolean remove(User user);
    Set<User> getAllUsers();
}
