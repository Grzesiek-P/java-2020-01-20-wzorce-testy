package pl.cm.homework.messenger.user.storage;

import pl.cm.homework.messenger.user.model.User;

import java.util.HashSet;
import java.util.Set;

public class UserStorageMemory implements UserStorage {

    Set<User> users = new HashSet<>();

    @Override
    public boolean add(User user) {
        return users.add(user);
    }

    @Override
    public boolean remove(User user) {
        return users.remove(user);
    }

    @Override
    public Set<User> getAllUsers() {
        return users;
    }
}
