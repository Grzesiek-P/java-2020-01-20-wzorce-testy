package pl.cm.homework.messenger.user.storage;

import pl.cm.homework.messenger.user.model.User;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserStorageLocal implements UserStorage {
    private static final Logger log = Logger.getLogger(UserStorageLocal.class.getName());

    private final File storage;


    public UserStorageLocal(File storage)  {
        this.storage = storage;
        if (!this.storage.exists()) {
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.storage))){
                oos.writeInt(0);

            } catch (IOException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }

    @Override
    public boolean add(User user) {
        Set<User> users = new HashSet<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storage))) {
            if (ois.readInt() != 0) {
                users = (Set<User>) ois.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }

        boolean result = users.add(user);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storage))) {
            oos.writeInt(users.size());
            oos.writeObject(users);
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public boolean remove(User user) {
        Set<User> users = new HashSet<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storage))) {
            if (ois.readInt() != 0) {
                users = (Set<User>) ois.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }

        boolean result = users.remove(user);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storage))) {
            oos.writeInt(users.size());
            oos.writeObject(users);
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public Set<User> getAllUsers() {
        Set<User> users = new HashSet<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storage))) {
            if (ois.readInt() != 0) {
                users = (Set<User>)ois.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return users;
    }
}
