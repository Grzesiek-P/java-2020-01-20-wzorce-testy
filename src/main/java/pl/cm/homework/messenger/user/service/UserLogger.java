package pl.cm.homework.messenger.user.service;

import pl.cm.homework.messenger.user.model.User;
import pl.cm.homework.messenger.user.storage.UserStorage;

import java.util.Scanner;

public class UserLogger {
    private final UserStorage userStorage;
    private final Scanner scanner;

    private User user;

    public UserLogger(UserStorage userStorage, Scanner scanner) {
        this.userStorage = userStorage;
        this.scanner = scanner;
    }

    public User getLoggedUser() {
        getLoginFromUser();
        addUserIfAbsent();
        System.out.println("Hello, " + user.getLogin() + "!");
        return user;
    }

    private void getLoginFromUser() {
        System.out.println("Login:");
        String login = scanner.nextLine();
        user = new User(login);
    }

    private void addUserIfAbsent() {
        if (!userStorage.getAllUsers().contains(user)) {
            userStorage.add(user);
            System.out.println("No such user in database. User created.");
        }
    }
}
