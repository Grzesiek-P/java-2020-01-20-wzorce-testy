package pl.cm.homework.messenger.user.storage;

import static org.assertj.core.api.Assertions.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.cm.homework.messenger.message.model.Message;
import pl.cm.homework.messenger.message.storage.MessageStorageLocal;
import pl.cm.homework.messenger.user.model.User;

import java.io.*;
import java.time.LocalDateTime;


/*
As mockito cannot mock final methods in ObjectInputStream and ObjectOutputStream,
I have decided to make this more of an integration test
 */

public class UserStorageLocalTest {


    private File file = new File("user-storage-test");


    private User user1 = new User("user1");
    private User user2 = new User("user2");

    @Before
    public void before() throws Exception {
        file.delete();
    }

    @Test
    public void add_remove_getAllUsers_IntegrationTest() throws IOException {
        UserStorageLocal storage = new UserStorageLocal(file);

        assertThat(file.exists()).isTrue();

        Boolean resultAdd1 = storage.add(user1);
        Boolean resultAdd2 = storage.add(user2);
        Boolean resultAdd3 = storage.add(user2);

        assertThat(storage.getAllUsers()).containsExactlyInAnyOrder(user1, user2);
        assertThat(resultAdd1).isTrue();
        assertThat(resultAdd2).isTrue();
        assertThat(resultAdd3).isFalse();

        Boolean resultRemove1 = storage.remove(user2);
        Boolean resultRemove2 = storage.remove(user2);

        assertThat(storage.getAllUsers()).containsExactly(user1);
        assertThat(resultRemove1).isTrue();
        assertThat(resultRemove2).isFalse();


    }

    @After
    public void after() {
        file.delete();
    }

}