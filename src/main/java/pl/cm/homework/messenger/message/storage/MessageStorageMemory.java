package pl.cm.homework.messenger.message.storage;

import pl.cm.homework.messenger.message.model.Message;

import java.util.LinkedList;
import java.util.List;

public class MessageStorageMemory implements MessageStorage {

    private List<Message> messages = new LinkedList<>();
    @Override
    public boolean add(Message message) {
        return messages.add(message);
    }

    @Override
    public boolean remove(Message message) {
        return messages.remove(message);
    }

    @Override
    public List<Message> getAllMessages() {
        return messages;
    }
}
