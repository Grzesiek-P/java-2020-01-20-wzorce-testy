package pl.cm.homework.messenger.message.printer;

import pl.cm.homework.messenger.message.model.Message;

import java.util.List;

public interface MessagePrinter {

    void print(Message message);

    default void printList(List<Message> list) {
        if (list.isEmpty()) {
            System.out.println("No messages to print");
        } else {
            for (Message message : list) {
                print(message);
            }
        }
    }
}
